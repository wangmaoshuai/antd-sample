// @ts-ignore
/* eslint-disable */

declare namespace API {
  type CurrentUser = {
    name?: string;
    avatar?: string;
    userid?: string;
    email?: string;
    signature?: string;
    title?: string;
    group?: string;
    tags?: { key?: string; label?: string }[];
    notifyCount?: number;
    unreadCount?: number;
    country?: string;
    access?: string;
    geographic?: {
      province?: { label?: string; key?: string };
      city?: { label?: string; key?: string };
    };
    address?: string;
    phone?: string;
  };

  type LoginResult = {
    status?: string;
    type?: string;
    currentAuthority?: string;
  };

  type PageParams = {
    current?: number;
    pageSize?: number;
  };

  type RuleListItem = {
    key?: number;
    disabled?: boolean;
    href?: string;
    avatar?: string;
    name?: string;
    owner?: string;
    desc?: string;
    callNo?: number;
    status?: number;
    updatedAt?: string;
    createdAt?: string;
    progress?: number;
  };

  type RuleList = {
    data?: RuleListItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type FakeCaptcha = {
    code?: number;
    status?: string;
  };

  type LoginParams = {
    username?: string;
    password?: string;
    autoLogin?: boolean;
    type?: string;
    isSystem?: boolean;
  };

  type ErrorResponse = {
    /** 业务约定的错误码 */
    errorCode: string;
    /** 业务上的错误信息 */
    errorMessage?: string;
    /** 业务上的请求是否成功 */
    success?: boolean;
  };

  type NoticeIconList = {
    data?: NoticeIconItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type NoticeIconItemType = 'notification' | 'message' | 'event';

  type NoticeIconItem = {
    id?: string;
    extra?: string;
    key?: string;
    read?: boolean;
    avatar?: string;
    title?: string;
    status?: string;
    datetime?: string;
    description?: string;
    type?: NoticeIconItemType;
  };
  type UserAdminRole = {
    adminSession?: Session;
    beginDate?: string;
    beginInclusive?: boolean;
    beginLockDate?: string;
    beginRange?: string;
    beginTime?: string;
    contextId?: string;
    dayMask?: string;
    endDate?: string;
    endInclusive?: boolean;
    endLockDate?: string;
    endRange?: string;
    endTime?: string;
    modCode?: string;
    modId?: string;
    name?: string;
    osPSet?: [string];
    osUSet?: [string];
    parents?: [string];
    rawData?: string;
    roleRangeRaw?: string;
    sequenceId?: number;
    temporalSet?: boolean;
    timeout?: number;
    userId?: string;
  }

  type Group = {
    adminSession?: Session;
    children?: [string];
    contextId?: string;
    createTime?: string;
    description?: string;
    gidNumber?: string;
    heritableRoles?: [string];
    memberDn?: boolean;
    members?: [string];
    modCode?: string;
    modId?: string;
    modifyTime?: string;
    name?: string;
    owners?: [string];
    parents?: [string];
    propList?: [string];
    properties?: {};
    props?: Props;
    protocol?: string;
    roles?: [string];
    sequenceId?: number;
  }

  type Props = {
    adminSession?: Session;
    contextId?: string;
    entry?: [Entry];
    modCode?: string;
    modId?: string;
    sequenceId?: number;
  }

  type Session = {
    adminRoles?: [UserAdminRole];
    adminSession?: Session;
    authenticated?: boolean;
    clientIp?: string;
    contextId?: string;
    errorId?: number;
    expirationSeconds?: number;
    graceLogins?: number;
    internalUserId?: string;
    lastAccess?: number;
    modCode?: string;
    modId?: string;
    msg?: string;
    roles?: [UserRole];
    sequenceId?: number;
    serverIp?: string;
    sessionId?: string;
    user?: User;
    userId?: string;
    warnings?: [Warning];
  }

  type Warning = {
    id?: number;
    msg?: string;
    name?: string;
    type?: 'ROLE' | 'PASSWORD';
  }

  type Entry = {
    key?: string;
    value?: string;
  }

  type Address = {
    addresses?: [string];
    building?: string;
    city?: string;
    country?: string;
    departmentNumber?: string;
    postOfficeBox?: string;
    postalCode?: string;
    roomNumber?: string;
    state?: string;
  }

  type User = {
    address?: Address;
    adminRoles?: [UserAdminRole];
    adminSession?: Session;
    beginDate?: string;
    beginLockDate?: string;
    beginTime?: string;
    cn?: string;
    contextId?: string;
    createTime?: string;
    dayMask?: string;
    description?: string;
    displayName?: string;
    dn?: string;
    emails?: [string];
    employeeType?: string;
    endDate?: string;
    endLockDate?: string;
    endTime?: string;
    gecos?: string;
    gidNumber?: string;
    homeDirectory?: string;
    internalId?: string;
    jpegPhoto?: string;
    locked?: boolean;
    loginShell?: string;
    mobiles?: [string];
    modCode?: string;
    modId?: string;
    modifyTime?: string;
    name?: string;
    newPassword?: [string];
    ou?: string;
    password?: [string];
    phones?: [string];
    properties?: {};
    props?: Props;
    pwPolicy?: string;
    rawData?: string;
    reset?: boolean;
    roles?: [UserRole];
    sequenceId?: number;
    sn?: string;
    system?: boolean;
    temporalSet?: boolean;
    timeout?: number;
    title?: string;
    uidNumber?: string;
    userId?: string;
  }

  type UserRole = {
    adminSession?: Session;
    beginDate?: string;
    beginLockDate?: string;
    beginTime?: string;
    contextId?: string;
    dayMask?: string;
    endDate?: string;
    endLockDate?: string;
    endTime?: string;
    modCode?: string;
    modId?: string;
    name?: string;
    parents?: [string];
    rawData?: string;
    sequenceId?: number;
    temporalSet?: boolean;
    timeout?: number;
    userId?: string;
  }

  type SessionEntity = {
    adminRoles?: [UserAdminRole];
    authenticated?: boolean;
    expirationSeconds?: number;
    graceLoginTimes?: number;
    groups?: [Group];
    lastAccess?: number;
    message?: string;
    roles?: [UserRole];
    sessionId?: string;
    user?: string;
  }

  type TranswarpLoginResult = {
    user?: string;
    sessionId?: string;
    lastAccess?: number;
    expirationSeconds?: number;
    graceLoginTimes?: number;
    groups?: [Group];
    roles?: [UserRole];
    adminRoles?: [UserAdminRole];
    authenticated?: boolean;
  }
  type TranswarpCurrentUser = {
    authorities?: [{ authority?: string }];
    details?: {
      remoteAddress?: string;
      tenant?: string;
      additionalInformation?: {};
    },
    authenticated?: boolean;
    principal?: {
      username?: string;
      authorities?: [{ authority?: string }];
      accountNonExpired?: boolean;
      accountNonLocked?: boolean;
      credentialsNonExpired?: boolean;
      enabled?: boolean;
    },
    name?: string;
  }
}



