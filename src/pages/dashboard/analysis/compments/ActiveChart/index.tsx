import { Component } from 'react';
import { TinyArea } from '@ant-design/charts';

import { Statistic } from 'antd';
import styles from './index.less';
import { getActiveData } from '../../service';
import type { DataItem } from '../../data';


export default class ActiveChart extends Component {
  initData = () => {
    const result = [] as DataItem[];
    getActiveData().then((d) => {
      d.data.forEach((x) => result.push(x));
    });
    return result;
  };

  state = {
    activeData: this.initData(),
  };

  timer: number | undefined = undefined;

  requestRef: number | undefined = undefined;

  componentDidMount() {
    this.loopData();
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
    if (this.requestRef) {
      cancelAnimationFrame(this.requestRef);
    }
  }

  loopData = () => {
    this.requestRef = requestAnimationFrame(() => {
      this.timer = window.setTimeout(() => {
        getActiveData().then((data) => {
          this.setState({
            activeData: data.data,
          }, () => {
            this.loopData();
          })
        });
      }, 5000);
    });
  };

  render() {
    const { activeData = [] } = this.state;

    return (
      <div className={styles.activeChart}>
        <Statistic title="目标评估" value="有望达到预期" />
        <div style={{ marginTop: 32 }}>
          <TinyArea data={activeData} xField="x" forceFit yField="y" height={200} />
        </div>
        {activeData.length !== 0 && (
          <div>
            <div className={styles.activeChartGrid}>
              <p>{[...activeData].sort()[activeData.length - 1].y} 亿元</p>
              <p>{[...activeData].sort()[Math.floor(activeData.length / 2)].y} 亿元</p>
            </div>
            <div className={styles.dashedLine}>
              <div className={styles.line} />
            </div>
            <div className={styles.dashedLine}>
              <div className={styles.line} />
            </div>
          </div>
        )}
        {activeData.length !== 0 && (
          <div className={styles.activeChartLegend}>
            <span>00:00</span>
            <span>{activeData[Math.floor(activeData.length / 2)].x}</span>
            <span>{activeData[activeData.length - 1].x}</span>
          </div>
        )}
      </div>
    );
  }
}
