import type { FC } from 'react';
import { Suspense, useState } from 'react';
import { EllipsisOutlined } from '@ant-design/icons';
import { Card, Col, Dropdown, Menu, Row } from 'antd';
import { GridContent } from '@ant-design/pro-layout';
import type { RadioChangeEvent } from 'antd/es/radio';
import { useRequest } from 'umi';
import ProportionSales from './compments/ProportionSales';
import { Liquid ,WordCloud} from '@ant-design/charts';
import OfflineData from './compments/OfflineData';

import { fakeChartData, getResourceUseData, getOfflineData ,getWordData} from './service';
import type { AnalysisData } from './data.d';
import styles from './style.less';
import ActiveChart from './compments/ActiveChart';

type AnalysisProps = {
  dashboardAndanalysis: AnalysisData;
  loading: boolean;
};

type SalesType = 'all' | 'online' | 'stores';

const Analysis: FC<AnalysisProps> = () => {
  const [salesType, setSalesType] = useState<SalesType>('all');
  const [currentTabKey, setCurrentTabKey] = useState<string>('');

  const { data, loading } = useRequest(() => {
    return fakeChartData(salesType);
  }, {
    manual: false,
    debounceInterval: 500,
    refreshDeps: [salesType]
  });

  const wordData=useRequest(()=>{
    return getWordData();
  });

  const useageData = useRequest(() => {
    return getResourceUseData("default");
  });
  const salesPieData = data;
  const offlineData = useRequest(() => {
    return getOfflineData();
  }, {
    refreshDeps: [currentTabKey]
  });

  const menu = (
    <Menu>
      <Menu.Item>操作一</Menu.Item>
      <Menu.Item>操作二</Menu.Item>
    </Menu>
  );

  const handleTabChange = (key: string) => {
    setCurrentTabKey(key);
  };


  const dropdownGroup = (
    <span className={styles.iconGroup}>
      <Dropdown overlay={menu} placement="bottomRight">
        <EllipsisOutlined />
      </Dropdown>
    </span>
  );

  const handleChangeSalesType = (e: RadioChangeEvent) => {
    setSalesType(e.target.value);
  };

  const handleRollBack = () => {
    console.log("roll back!");
  }

  const activeKey = currentTabKey || (offlineData.data?.offlineData[0] && offlineData.data?.offlineData[0].name) || '';

  return (
    <GridContent>
      <>
        <Row
          gutter={24}
          style={{
            marginTop: 24,
          }}
        >
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Suspense fallback={handleRollBack}>
              <ProportionSales
                dropdownGroup={dropdownGroup}
                salesType={salesType}
                loading={loading}
                salesPieData={salesPieData || []}
                handleChangeSalesType={handleChangeSalesType}
              />
            </Suspense>
          </Col>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Card title="活动情况预测" style={{ marginBottom: 24 ,height: '100%', }} bordered={false}>
              <ActiveChart />
            </Card>
          </Col>

        </Row>
        <Row
          gutter={24}
          style={{
            marginTop: 24,
          }}
        >
          <Col xl={6} lg={12} sm={24} xs={24} style={{ marginBottom: 24 }}>
            <Card
              title="资源剩余"
              bodyStyle={{ textAlign: 'center', fontSize: 0 }}
              bordered={false}
            >
              <Liquid
                height={161}
                min={0}
                max={10000}
                value={useageData?.data ? useageData.data : 0}
                forceFit
                padding={[0, 0, 0, 0]}
                statistic={{
                  formatter: (value) => `${((100 * value) / 10000).toFixed(1)}%`,
                }}
              />
            </Card>
          </Col>
          <Col xl={6} lg={12} sm={24} xs={24} style={{ marginBottom: 24 }}>
            <Card
              title="热门搜索"
              loading={wordData.loading}
              bordered={false}
              bodyStyle={{ overflow: 'hidden' }}
            >
              <WordCloud
                data={wordData.data!}
                forceFit
                height={162}
                wordStyle={{
                  fontSize: [10, 20],
                }}
                shape="triangle"
              />
              {/* <TagCloud data={data?.list || []} height={161} /> */}
            </Card>
          </Col>
        </Row>
        <Suspense fallback={null}>
          <OfflineData
            activeKey={activeKey}
            loading={loading}
            offlineData={offlineData.data?.offlineData || []}
            offlineChartData={offlineData.data?.offlineChart || []}
            handleTabChange={handleTabChange}
          />
        </Suspense>
      </>
    </GridContent>
  );
};

export default Analysis;
