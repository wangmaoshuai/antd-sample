import { request } from 'umi';
import type { DataItem, OfflineDataType} from './data';
import type {WordCloudData} from '@antv/g2plot/esm/plots/word-cloud/layer';
export async function fakeChartData(type: string): Promise<{ data: DataItem[] }> {
  return request('/antd-app/api/sales', {
    method: 'GET',
    params: {
      type: type
    },
  });
}

export async function getActiveData(): Promise<{ data: DataItem[] }> {
  return request('/antd-app/api/active');
}

export async function getResourceUseData(resourceType: string, options?: Record<string, any>): Promise<{ data: number }> {
  return request('/antd-app/api/getResourceUseData', {
    method: 'GET',
    params: {
      sourceType: resourceType
    },
    ...(options || {}),
  });
}

export async function getOfflineData(): Promise<{
  data: {
    offlineData: OfflineDataType[],
    offlineChart: DataItem[]
  }
}> {
  return request('/antd-app/api/getOfflineData')
}

export async function getWordData(): Promise<{ data:  WordCloudData[] }> {
  return request('/antd-app/api/getWordData')
}
