import { PageContainer } from '@ant-design/pro-layout';
import { useState } from 'react';
import { Button, Input } from 'antd';
import type { FC } from 'react';
import Articles from './articles';
import useRequest from '@ahooksjs/use-request';
import { searchNews } from './articles/service';
import { LoadingOutlined } from '@ant-design/icons';
import { forEach } from '@umijs/deps/compiled/lodash';

type SearchProps = {
    match: {
        url: string;
        path: string;
    };
    location: {
        pathname: string;
    };
};
interface Item {
    id: string,
    title: string,
    content: string,
}
interface Result {
    list: Item[],
}

const SearchArticles: FC<SearchProps> = () => {
    const [searchKey, setSearchkey] = useState<string>('');

    const { data, loading, reload, loadMore, loadingMore } = useRequest((d: Result | undefined) => {
        const temp = d?.list || [];
        console.log(temp.length);
        return searchNews(searchKey, temp.length == 0 ? 0 : temp.length + 2, 2).then((x) => {
            return { list: x.data }
        });
    }, {
        manual: false,
        debounceInterval: 500,
        refreshDeps: [searchKey],
        loadMore: true,
        cacheKey: 'loadMoreDemoCacheId',
    });
    const list = data?.list || [];
    console.log(data);

    const onSearch = (value: string) => {
        reload();
        setSearchkey(value);
    };


    const loadMoreDom = list.length > 0 && (
        <div style={{ textAlign: 'center', marginTop: 16 }}>
            <Button onClick={loadMore} style={{ paddingLeft: 48, paddingRight: 48 }}>
                {loadingMore ? (
                    <span>
                        <LoadingOutlined /> 加载中...
                    </span>
                ) : (
                    '加载更多'
                )}
            </Button>
        </div>
    );

    return (
        <PageContainer>
            <>
                <Input.Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                />
            </>
            <Articles iterm={list} loading={loading} loadMoreDom={loadMoreDom} />
        </PageContainer>
    );
};

export default SearchArticles;
