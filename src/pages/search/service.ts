import { request } from 'umi';

export type News = {
  id: string,
  title: string,
  content: string,
}

export async function searchNews(
  searchKey: string, from: number, size: number
): Promise<{ data: News[] }> {
  return request('/antd-app/api/getIndexSource', {
    method: 'GET',
    params: {
      index: 'news_analyze_zh',
      from: from,
      size: size,
      searchKey: searchKey,
    }
  });
};