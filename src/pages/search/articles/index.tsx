import { LikeOutlined, MessageOutlined, StarOutlined } from '@ant-design/icons';
import { Card, List, Tag } from 'antd';
import type { FC } from 'react';
import React from 'react';
import ArticleListContent from './components/ArticleListContent';
import styles from './style.less';


const Articles: FC<{
  iterm: {
    id: string,
    title: string,
    content: string,
  }[], loading: boolean, loadMoreDom: false | JSX.Element,
}> = ({ iterm, loading, loadMoreDom }) => {


  const list = iterm || [];


  const IconText: React.FC<{
    type: string;
    text: React.ReactNode;
  }> = ({ type, text }) => {
    switch (type) {
      case 'star-o':
        return (
          <span>
            <StarOutlined style={{ marginRight: 8 }} />
            {text}
          </span>
        );
      case 'like-o':
        return (
          <span>
            <LikeOutlined style={{ marginRight: 8 }} />
            {text}
          </span>
        );
      case 'message':
        return (
          <span>
            <MessageOutlined style={{ marginRight: 8 }} />
            {text}
          </span>
        );
      default:
        return null;
    }
  };

  // const loadMoreDom = list!.length > 0 && (
  //   <div style={{ textAlign: 'center', marginTop: 16 }}>
  //     <Button onClick={loadMore} style={{ paddingLeft: 48, paddingRight: 48 }}>
  //       {loadingMore ? (
  //         <span>
  //           <LoadingOutlined /> 加载中...
  //         </span>
  //       ) : (
  //         '加载更多'
  //       )}
  //     </Button>
  //   </div>
  // );

  return (
    <>
      <Card
        style={{ marginTop: 24 }}
        bordered={false}
        bodyStyle={{ padding: '8px 32px 32px 32px' }}
      >
        <List<{
          id: string,
          title: string,
          content: string,
        }>
          size="large"
          loading={loading}
          rowKey="id"
          itemLayout="vertical"
          loadMore={loadMoreDom}
          dataSource={list}
          renderItem={(item) => (
            <List.Item
              key={item.id}
              actions={[
                <IconText key="star" type="star-o" text={'0'} />,
                <IconText key="like" type="like-o" text={'0'} />,
                <IconText key="message" type="message" text={'0'} />,
              ]}
              extra={<div className={styles.listItemExtra} />}
            >
              <List.Item.Meta
                title={
                  <a className={styles.listItemMetaTitle} href={''}>
                    {item.title}
                  </a>
                }
                description={
                  <span>
                    <Tag>Ant Design</Tag>
                    <Tag>设计语言</Tag>
                    <Tag>蚂蚁金服</Tag>
                  </span>
                }
              />
              <ArticleListContent data={item} />
            </List.Item>
          )}
        />
      </Card>
    </>
  );
};

export default Articles;
