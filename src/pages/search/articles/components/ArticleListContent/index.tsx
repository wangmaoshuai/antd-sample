import React from 'react';
import styles from './index.less';

type ArticleListContentProps = {
  data: {
    content: string;
  };
};

const ArticleListContent: React.FC<ArticleListContentProps> = ({
  data: { content},
}) => (
  <div className={styles.listContent}>
    <div className={styles.description}>{content}</div>
  </div>
);

export default ArticleListContent;
